# Data-generator-nautile

Ce projet participe à la démarche de mise en conformité de l'application portable afin de la sortir.


## Problématique


L'application Nautile manipule des données sensibles. Dans le démarche de portabilité de l'application, il est nécessaire d'avoir un jeu de tests générés aléatoirement pour ne pas exposer de données réelles.


## Description des objectifs définis par la problématique

- Pour les tests : besoin de générer des fichiers csv ou .sql de petite volumétrie,

- Pour le travail en local : besoin de générer un fichier regroupant plusieurs script en .sql d'initialisation des données au lancement du serveur.

- Pour l'externalisation : besoin de générer une quantité importante de données en .sql / .dump pour l'intégrer dans une base postgres dimensionnée comme en production.
(et également : pour le travail sur les serveurs de l'innovation en interne / externe ))



### Discussion avec Ludovic Vincent

=> Reprise de données géographiques pour effectuer le lien entre les données générées. Donc extraction de données réelles et application des liens sur une génération de données propres.

## Developpement - description des étapes
:one:

- Redaction du script de génération du json à taille variable.
:heavy_check_mark:

:two:
- Développement d'un module java pour récupérer ces données, les retravailler et générer un fichier .sql / trouver un existant :no_entry_sign:
- generation des données en json, conversion en .sql et .csv a partir d'outils libres en ligne. :heavy_check_mark:
- Intégration en self des données pour les tests en effectuant une reprise sur certaines données non sensibles : idMétiers, dirnoseq, dirindik ainsi que : reg et marquage. :heavy_check_mark:

:three:

- envoi du fichier .sql au prestataire par leur dépot :x:

- Cucumber : mise a jour des résultats des tests qui fonctionnent avec les données du fichier :heavy_check_mark:

- Base de données embarquée (postgres -> H2) : mise en place du script sql sur le module web et le module batch.  :x:

:triangular_flag_on_post:
=> Application fonctionnelle et intégrable à l'extérieur sans adhérences INSEE avec données anonymisées 

:four:
- mise en place d'une solution automatisée adhérente au module Model de Nautile pour la génération de données des tables Logement/Individu/Adresse :x:


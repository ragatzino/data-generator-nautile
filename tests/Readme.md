# Anonymisation pour les tests
<p>
<b>Géneration de données pour l'externalisation , le travail en local, les tests fonctionnels sur Nautile</b>
</p>

## Description de la solution

### Étude de solutions existantes de génération de données
- génération par définition depuis le java : ce serait trop couteux en temps mais ça pourrait être faire sur certaines données complexes à générer

- https://www.json-generator.com/ est une offre gratuite de génération de données, en json avec quelques petites fonctionnalités pour faire apparaitre certains comportements a la génération.

- https://mockaroo.com/ est une réference dans la génération de données, sa dimension payante/privée est contraignante. On peut toutefois envisager de générer des données de tests à hauteur de 1000 lignes.


#### **https://www.json-generator.com/**


- Avantages :

Facile à prendre en main, maitrise de l'outil, maitrise des données et des comportements simples, possibilité de versionner le fichier menant a la génération.



- Inconvénients : 

Reprise des données existantes pour la géolocalisation ou au moins travail en self à effectuer pour faire les liens relationnels.


#### **https://mockaroo.com/**

L'offre gratuite donne accès à la génération de 1000 lignes par execution.


Cela donnerait : 200 adresses => 500 logements => 1000 individus. Donc solution maison :  en executant une macro on pourrait récupérer un certain volume de données

- Avantages :

Liens relationnels déjà effectués => moins de travail et moins de revelations sur les données. 

- Inconvénients : 

propriétaire
méthode limitée,
évolution couteuse


#### ****Choix final****

Entre deux : génération de volumes moyens avec retravail des données

Le choix a été fait de prendre la solution json-generator avec retravail des données ensuite pour plus de practicité et par respect pour les offres gratuites.
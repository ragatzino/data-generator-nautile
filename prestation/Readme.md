# Proposition pour la prestation 

Recherche en cours de moyens de générations gros volume intégrés en interne, pour l'instant pas de bilan positif. Solutions clés en main payantes ..

## Problématique 

Proposer une génération cohérente des données au prestataire

## Solutions

### Interne

Récupération d'identifiants techniques et du dessin de fichier de fidéli n. Generation de données puis ajout de colonnes avec les identifiants déjà reliés entre eux en bdd. 

### Externe

Envoi du dessin de fichier de fidéli n, génération avec un outil permettant la gestion clé primaire/ étrangères pour les volumétries suivantes : 

```
Adresse : 26 000 000
Logement : 41 000 000
Individu : 69 000 000
```

#### Cas particuliers lors de la génération 

Logements biloc : certains individus ont plus d'un logement qu'ils relient avec l'id_log_biloc.

Il faudra donc générer des valeurs sur les variables en *_biloc dans individu quand ce sera le cas.

